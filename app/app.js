'use strict';

angular.module('app', [])
    .controller('BooksController', function () {
        var ctrl = this;

        ctrl.rows = [
            {"title": "La Horde du Contrevent", "author": "Alain Damasio",  "price": 10.90, quantity: 3},
            {"title": "Black-out",              "author": "Connie Willis",  "price": 9.90,  quantity: 2}
        ];


    })
;

